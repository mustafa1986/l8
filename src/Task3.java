import java.util.Arrays;
import java.util.Scanner;

public class Task3 {
    public static void task3() {
        //•	Cümlə daxil edin. Əgər cümlə saitlə başlayıb,samitlə bitirsə cümlədə sözlərin yerini simmetrik dəyişin.
        // (məsələn:saitlə başlayıb samitlə bitirsə 1-ci və 4-cü sözün ,2-ci ilə 3-cü sözün yerini dəyişin).
        Scanner scanner = new Scanner(System.in);
        System.out.println("Metni daxil edin: ");
        String metn = scanner.nextLine();
        String[] metnArray = metn.split(" ");
        String saitler = "aeiou";
        char firstSymbol = metn.charAt(0);
        char lastSymbol = metn.charAt(metn.length() - 1);
        if (saitler.indexOf(firstSymbol) != -1 && saitler.indexOf(lastSymbol) == -1) {
            for (int i = 0; i < metnArray.length / 2; i++) {
                String temp = metnArray[i];
                metnArray[i] = metnArray[metnArray.length - 1 - i];
                metnArray[metnArray.length - 1 - i] = temp;
            }

            System.out.println("Symmetric deyishikden sonra ");
            String newMetn= String.join(" ",metnArray);
            System.out.println(newMetn);
        } else {
            System.out.println("Symmetric deyishiklik ucun Cumle saitle bashlamalidi samitle bitmelidi");
        }


    }
}
