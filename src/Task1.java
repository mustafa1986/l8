import java.util.Scanner;

public class Task1 {
    public static void task1() {
        //•	[Unversitet] [metro] metrosunda [küçə]
        // Cümləsində [Unversitet] [metro] və [küçə] sözlərini uyğun unversitet,
        // metro və küçə adları ilə  əvəz edin.
        Scanner scanner = new Scanner(System.in);
        String metn = "[Unversitet] [metro] metrosunda [küçə] küçəsində yerləşir";
        System.out.println("Universitet adin daxil edin: ");
        String universitetName = scanner.nextLine();
        if (universitetName.length() <= 1) {
            System.out.println("Universitet adin duzgun Daxil edin! ");
            return;
        }
        System.out.println("Metro stansiyasin daxil edin: ");
        String metroName= scanner.nextLine();
        if (metroName.length()<=3 ){
            System.out.println("Metro adi duzgun deyil!");
            return;
        }
        System.out.println("Kuce adin duzgun daxil edin: ");
        String kuceAdi = scanner.nextLine();
        if (kuceAdi.length()<=3){
            System.out.println("Kuce adi 3 herfden kicik ola bilmez!");
            return;
        }
        metn= metn.replace("[Unversitet]",universitetName).replace("[metro]", metroName).replace("[küçə]",kuceAdi);
        System.out.println("Yeni metn: " +metn);


    }

}
