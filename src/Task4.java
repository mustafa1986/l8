import java.util.Arrays;
import java.util.Scanner;

public class Task4 {
    public static void task4() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Metni daxil edin: ");
        String metn = scanner.nextLine();
        char metnArray[] = metn.toCharArray();
        for (int i = 0; i < metnArray.length; i++) {
            if (metnArray[i] >= 'a' && metnArray[i] <= 'z') {
                metnArray[i] = (char) ((int) metnArray[i] - 32);
            } else if (metnArray[i] >= 'A' && metnArray[i] <= 'Z') {
                metnArray[i] = (char) ((int) metnArray[i] + 32);
            }

        }
        System.out.println("Daxil etdiyiniz metn:\n" + metn);
        //arashdirmaq lazimdi
        //System.out.println("Deyisiklikden sonra metn:\n " + metnArray);???????????????
        System.out.println("Deyisiklikden sonra metn:");
        System.out.println(metnArray);


    }
}
