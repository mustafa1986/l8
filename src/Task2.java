import java.util.Scanner;

public class Task2 {
    public static void task2() {
        //•	Her hansi bir cumle daxil edilecek.Hemin cumlede olan samitlerin sayini tapmaq teleb olunur.
        // Ve hemin samitlerin yerlesdiyi index-lerin cemini tapmaq teleb olunur.
        //Daxil edilmis cumlede daha cox samit olan soz cap edilsin ve oradaki samitlerin sayi cap edilsin.

        Scanner scanner = new Scanner(System.in);
        int saitCount = 0;
        int samitCount = 0;
        int sumIndex = 0;
        int maksSamit = 0;
        int counter =0 ;
        String sammitliSoz = "";
        String sait = "aeiou";
        System.out.println("Metni daxil edin: ");
        String metn = scanner.nextLine();
        metn = metn.toLowerCase();
        String[] sozler = metn.split("\\s+");
        for (int i = 0; i < metn.length(); i++) {
            if ((metn.charAt(i)) >= 'a' && metn.charAt(i) <= 'z') {
                if (sait.indexOf(metn.charAt(i)) != -1)
                    saitCount++;
                else {
                    samitCount++;
                    sumIndex += metn.indexOf(metn.charAt(i));
                }
            }
        }

        for (int i = 0; i < sozler.length; i++) {
            counter=0;
            for (int j = 0; j < sozler[i].length(); j++) {
                if ((sozler[i].charAt(j)) >= 'a' && sozler[i].charAt(j) <= 'z') {
                    if (sait.indexOf(sozler[i].charAt(j)) != -1)
                        continue;
                    else {
                        counter++;
                        if (counter>maksSamit){
                            maksSamit=counter;
                            sammitliSoz=sozler[i];
                        }
                    }
                }
            }
        }
        System.out.println("Sait sesler: " + saitCount + "\nSamit Sesler: " + samitCount + "\nIndex sayi: " + sumIndex);
        System.out.println("samiti cox olan Soz: " + sammitliSoz + "\nSozde samit sayi: " + maksSamit);
    }
}
